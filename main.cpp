#include "gui.h"
#include "shader.h"
#include "window.h"

#include <fstream>
#include <chrono>

int main()
{
    Window window;
    GUI gui(window.pointer);

    ImGuiIO &io = ImGui::GetIO();
    io.FontGlobalScale = 1.f / std::min(window.xscale, window.yscale);
    io.Fonts->AddFontFromFileTTF(FONT_DIR "Roboto-Medium.ttf", 16.f / io.FontGlobalScale, nullptr, io.Fonts->GetGlyphRangesCyrillic());

    auto const readFile = [](char const * const filename) noexcept
    {
        std::ifstream in(filename);
        char buffer[16384] = {'\0'};
        in.read(buffer, sizeof(buffer));
        return std::string(buffer);
    };
    Shader const shader =
    {
        readFile(SHADER_DIR "vertex.glsl"  ).c_str(),
        readFile(SHADER_DIR "fragment.glsl").c_str(),
    };
    
    unsigned int vao;
    glGenVertexArrays(1, &vao);

    auto const t0 = std::chrono::steady_clock::now();

    glEnable(GL_DEPTH_TEST);
    while(!window.shouldClose())
    {
        int width, height;
        glfwGetFramebufferSize(window.pointer, &width, &height);

        glClearColor(0.1f, 0.1f, 0.3f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(shader);

        {
            auto const t = std::chrono::steady_clock::now();
            std::chrono::duration<float, std::ratio<1>> const dt = t - t0;
            glUniform1f(glGetUniformLocation(shader, "uTime"), dt.count());
            glUniform3f(glGetUniformLocation(shader, "uMouse"), gui.mouseX, gui.mouseY, gui.mouseZ);
            glUniform1f(glGetUniformLocation(shader, "uAspectRatio"), float(width) / float(height));
        }

        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, 12);
        glBindVertexArray(0);

        gui.render();
    }
}
